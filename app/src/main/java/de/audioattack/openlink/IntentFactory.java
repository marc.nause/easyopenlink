/*
 * SPDX-FileCopyrightText: 2017 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.openlink;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.Collection;

import de.audioattack.openlink.conf.RequirementsChecker;

public class IntentFactory {

    private static final String TAG = IntentFactory.class.getSimpleName();

    /**
     * Private constructor to avoid instantiation of static utility class.
     */
    private IntentFactory() {
    }

    public static Intent createIntent(@NonNull final Context context, @NonNull final Uri uri, final boolean incognito) {

        final Intent intent;

        if (incognito) {

            final String scheme = uri.getScheme();

            if ("http".equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme)) {

                final PackageInfo packageInfo = getAppWhichHandlesHttp(context);
                final IncognitoBrowser browser = RequirementsChecker.getIncognitoBrowser(packageInfo);

                if (browser != null) {
                    intent = new Intent(Intent.ACTION_VIEW, uri);
                    if (browser.incognitoExtra != null) {
                        intent.putExtra(browser.incognitoExtra, true);
                    }
                } else {
                    intent = getIntent(context, uri);
                }

            } else {
                intent = new Intent(Intent.ACTION_VIEW, uri);
            }

        } else {
            intent = new Intent(Intent.ACTION_VIEW, uri);
        }

        return intent;


    }

    private static Intent getIntent(@NonNull final Context context, @NonNull final Uri uri) {

        final Intent intent = new Intent(Intent.ACTION_VIEW);
        final Collection<IncognitoBrowser> browsers = RequirementsChecker.getIncognitoBrowsers(context);
        /*
         * TODO present selection to user if more than one browser is returned here
         *
         * See: https://codeberg.org/marc.nause/easyopenlink/issues/14
         */

        final IncognitoBrowser browser = browsers.iterator().next();
        intent.setClassName(browser.packageName, browser.browserActivityName);
        intent.putExtra(browser.incognitoExtra, true);
        intent.setData(uri);

        return intent;
    }

    private static PackageInfo getAppWhichHandlesHttp(@NonNull final Context context) {

        final PackageManager packageManager = context.getPackageManager();

        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://example.org"));

        ComponentName info = intent.resolveActivity(packageManager);

        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(info.getPackageName(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            Log.w(TAG, "Unable to get app which handles HTTPS.", e);
        }

        return packageInfo;
    }


}

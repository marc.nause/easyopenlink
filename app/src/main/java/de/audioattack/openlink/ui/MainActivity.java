/*
 * SPDX-FileCopyrightText: 2014 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.openlink.ui;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.text.HtmlCompat;

import de.audioattack.openlink.BuildConfig;
import de.audioattack.openlink.R;

public class MainActivity extends Activity {

    @Override
    protected final void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button btnVideo = findViewById(R.id.btn_video);
        btnVideo.setOnClickListener(v -> openUri(Uri.parse(getString(R.string.url_video))));

        final Button btnSource = findViewById(R.id.btn_source_code);
        btnSource.setOnClickListener(v -> openUri(Uri.parse(getString(R.string.url_source))));

        final TextView tvVersion = findViewById(R.id.version);
        tvVersion.setText(getString(R.string.lbl_version, BuildConfig.VERSION_NAME));

        final TextView tvCopyright = findViewById(R.id.copyright);
        tvCopyright.setText(HtmlCompat.fromHtml(getString(R.string.tv_copyright), HtmlCompat.FROM_HTML_MODE_LEGACY));
        tvCopyright.setMovementMethod(LinkMovementMethod.getInstance());

        final TextView tvUsedLibraries = findViewById(R.id.used_libraries);
        tvUsedLibraries.setText(HtmlCompat.fromHtml(getString(R.string.tv_used_libraries), HtmlCompat.FROM_HTML_MODE_LEGACY));
        tvUsedLibraries.setMovementMethod(LinkMovementMethod.getInstance());

    }

    private void openUri(@NonNull final Uri uri) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, uri));
        } catch (SecurityException | ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), getString(R.string.error_unable_to_open, uri.toString()), Toast.LENGTH_LONG)
                    .show();
        }
    }
}

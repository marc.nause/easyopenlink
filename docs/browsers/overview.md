# List of Web Browsers

[Easy Open Link](../) can open links in private tabs if a web browser is installed which supports opening private tabs via [Intents](https://developer.android.com/reference/android/content/Intent). This page is an ongoing attempt of keeping track of browsers for Android and their capabilities.

<table>
<tr>
    <th>name</th>
    <th>package name</th>
    <th>source code available</th>
    <th>incognito tabs via Intents</th>
    <th>from version</th>
    <th>to version</th>
    <th>Activity</th>
    <th>extra</th>
    <th>active project</th>
</tr>
<tr>
    <td>Brave Browser</td><!-- name -->
    <td>com.brave.browser</td><!-- package name -->
    <td><a href="https://github.com/brave/brave-browser">yes</a></td><!-- source code available -->
    <td>no</td><!-- incognito tabs via Intents -->
    <td></td><!-- from version -->
    <td></td><!-- to version -->
    <td></td><!-- Activity -->
    <td></td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>DuckDuckGo Privacy Browser</td><!-- name -->
    <td>com.duckduckgo.app.browser</td><!-- package name -->
    <td><a href="https://github.com/duckduckgo/Android">yes</a></td><!-- source code available -->
    <td>no</td><!-- incognito tabs via Intents -->
    <td></td><!-- from version -->
    <td></td><!-- to version -->
    <td></td><!-- Activity -->
    <td></td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>Fennec F-Droid (new)</td><!-- name -->
    <td>org.mozilla.fennec_fdroid</td><!-- package name -->
    <td><a href="https://hg.mozilla.org/releases/mozilla-esr68/">yes</a></td><!-- source code available -->
    <td>yes</td><!-- incognito tabs via Intents -->
    <td>112.0<br>1120000</td><!-- from version -->
    <td></td><!-- to version -->
    <td>org.mozilla.gecko.LauncherActivity</td><!-- Activity -->
    <td>private_browsing_mode</td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>Fennec F-Droid (old)</td><!-- name -->
    <td>org.mozilla.fennec_fdroid</td><!-- package name -->
    <td><a href="https://hg.mozilla.org/releases/mozilla-esr68/">yes</a></td><!-- source code available -->
    <td>yes</td><!-- incognito tabs via Intents -->
    <td>55.0.0<br>550000</td><!-- from version -->
    <td>68.12.0<br>689420</td><!-- to version -->
    <td>org.mozilla.gecko.LauncherActivity</td><!-- Activity -->
    <td>private_tab</td><!-- extra -->
    <td>no, <a href="https://forum.f-droid.org/t/welcome-a-new-fennec-f-droid/11113">superseded by Fenix based version</a></td><!-- active project -->
</tr>
<tr>
    <td>Firefox Fennec</td><!-- name -->
    <td>org.mozilla.firefox</td><!-- package name -->
    <td><a href="https://github.com/mozilla/gecko-dev">yes</a></td><!-- source code available -->
    <td>yes</td><!-- incognito tabs via Intents -->
    <td>55.0.0<br>2015503969</td><!-- from version -->
    <td>68.11.0<br>2015711849</td><!-- to version -->
    <td>org.mozilla.gecko.LauncherActivity</td><!-- Activity -->
    <td>private_tab</td><!-- extra -->
    <td>no, superseded by Firefox Fenix</td><!-- active project -->
</tr>
<tr>
    <td>Firefox Fenix</td><!-- name -->
    <td>org.mozilla.firefox</td><!-- package name -->
    <td><a href="https://github.com/mozilla-mobile/fenix/">yes</a></td><!-- source code available -->
    <td>yes</td><!-- incognito tabs via Intents -->
    <td>112.0<br>2015944785</td><!-- from version -->
    <td></td><!-- to version -->
    <td>org.mozilla.gecko.LauncherActivity</td><!-- Activity -->
    <td>private_browsing_mode</td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>Firefox Focus (aka Klar)</td><!-- name -->
    <td>org.mozilla.focus</td><!-- package name -->
    <td><a href="https://github.com/mozilla-mobile/focus-android">yes</a></td><!-- source code available -->
    <td>no</td><!-- incognito tabs via Intents -->
    <td></td><!-- from version -->
    <td></td><!-- to version -->
    <td></td><!-- Activity -->
    <td></td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>Firefox Lite (aka Rocket)</td><!-- name -->
    <td>org.mozilla.firefox</td><!-- package name -->
    <td><a href="https://github.com/mozilla-tw/FirefoxLite/">yes</a></td><!-- source code available -->
    <td>yes</td><!-- incognito tabs via Intents -->
    <td>2.0.0<br>16697 (maybe earlier)</td><!-- from version -->
    <td></td><!-- to version -->
    <td>org.mozilla.rocket.activity.PrivateModeActivity</td><!-- Activity -->
    <td></td><!-- extra -->
    <td>yes?</td><!-- active project -->
</tr>
<tr>
    <td>FOSS Browser</td><!-- name -->
    <td>de.baumann.browser</td><!-- package name -->
    <td><a href="https://github.com/scoute-dich/browser/">yes</a></td><!-- source code available -->
    <td>no</td><!-- incognito tabs via Intents -->
    <td></td><!-- from version -->
    <td></td><!-- to version -->
    <td></td><!-- Activity -->
    <td></td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>Icecat (new)</td><!-- name -->
    <td>org.gnu.icecat</td><!-- package name -->
    <td><a href="https://git.savannah.gnu.org/cgit/gnuzilla.git/">yes</a></td><!-- source code available -->
    <td>yes</td><!-- incognito tabs via Intents -->
    <td>115.3<br>1150000 (?)</td><!-- from version -->
    <td></td><!-- to version -->
    <td>org.mozilla.gecko.LauncherActivity</td><!-- Activity -->
    <td>private_browsing_mode</td><!-- extra -->
    <td>???</td><!-- active project -->
</tr>
<tr>
    <td>Icecat (old)</td><!-- name -->
    <td>org.gnu.icecat</td><!-- package name -->
    <td><a href="https://git.savannah.gnu.org/cgit/gnuzilla.git/">yes</a></td><!-- source code available -->
    <td>yes</td><!-- incognito tabs via Intents -->
    <td>55.0.0<br>550000</td><!-- from version -->
    <td>68.12.0<br>689420</td><!-- to version -->
    <td>org.mozilla.gecko.LauncherActivity</td><!-- Activity -->
    <td>private_tab</td><!-- extra -->
    <td>???</td><!-- active project -->
</tr>
<tr>
    <td>Jelly</td><!-- name -->
    <td>org.lineageos.jelly</td><!-- package name -->
    <td><a href="https://github.com/LineageOS/android_packages_apps_Jelly">yes</a></td><!-- source code available -->
    <td>yes</td><!-- incognito tabs via Intents -->
    <td>1.0<br>1</td><!-- from version -->
    <td></td><!-- to version -->
    <td>org.lineageos.jelly.MainActivity</td><!-- Activity -->
    <td>extra_incognito</td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>Jelly</td><!-- name -->
    <td>org.droidtr.jelly</td><!-- package name -->
    <td><a href="https://gitlab.com/droidtr/org.droidtr.jelly">yes</a></td><!-- source code available -->
    <td>yes</td><!-- incognito tabs via Intents -->
    <td>1.0<br>1</td><!-- from version -->
    <td></td><!-- to version -->
    <td>org.droidtr.jelly.MainActivity</td><!-- Activity -->
    <td>extra_incognito</td><!-- extra -->
    <td>no? (last commit in 2018)</td><!-- active project -->
</tr>
<tr>
    <td>jQuarks</td><!-- name -->
    <td>com.oF2pks.jquarks</td><!-- package name -->
    <td><a href="https://gitlab.com/oF2pks/jelly">yes</a></td><!-- source code available -->
    <td>yes</td><!-- incognito tabs via Intents -->
    <td>1.0<br>1</td><!-- from version -->
    <td></td><!-- to version -->
    <td>org.lineageos.jelly.MainActivity</td><!-- Activity -->
    <td>extra_incognito</td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>Lightning</td><!-- name -->
    <td>acr.browser.lightning</td><!-- package name -->
    <td><a href="https://github.com/anthonycr/Lightning-Browser">yes</a></td><!-- source code available -->
    <td>yes</td><!-- incognito tabs via Intents -->
    <td>5.0.0<br>97<br>(maybe earlier)</td><!-- from version -->
    <td></td><!-- to version -->
    <td>acr.browser.lightning.IncognitoActivity</td><!-- Activity -->
    <td></td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>Lucid Browser</td><!-- name -->
    <td></td><!-- package name -->
    <td><a href="https://github.com/powerpoint45/Lucid-Browser">yes</a></td><!-- source code available -->
    <td>no</td><!-- incognito tabs via Intents -->
    <td></td><!-- from version -->
    <td></td><!-- to version -->
    <td></td><!-- Activity -->
    <td></td><!-- extra -->
    <td>no</td><!-- active project -->
</tr>
<tr>
    <td>mBrowser</td><!-- name -->
    <td></td><!-- package name -->
    <td><a href="https://github.com/chelovek84/mBrowser">yes</a></td><!-- source code available -->
    <td>no</td><!-- incognito tabs via Intents -->
    <td></td><!-- from version -->
    <td></td><!-- to version -->
    <td></td><!-- Activity -->
    <td></td><!-- extra -->
    <td>no</td><!-- active project -->
</tr>
<tr>
    <td>Midori</td><!-- name -->
    <td>org.midorinext.android</td><!-- package name -->
    <td><a href="https://gitlab.com/midori-web/midori-android">yes</a></td><!-- source code available -->
    <td>yes</td><!-- incognito tabs via Intents -->
    <td>1.1.0<br>6</td><!-- from version -->
    <td></td><!-- to version -->
    <td>org.midorinext.android.IncognitoActivity"</td><!-- Activity -->
    <td></td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>Opera</td><!-- name -->
    <td>com.opera.browser</td><!-- package name -->
    <td>no</td><!-- source code available -->
    <td>???</td><!-- incognito tabs via Intents -->
    <td>???</td><!-- from version -->
    <td>???</td><!-- to version -->
    <td>???</td><!-- Activity -->
    <td>???</td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>Opera Mini</td><!-- name -->
    <td>com.opera.mini.native</td><!-- package name -->
    <td>no</td><!-- source code available -->
    <td>???</td><!-- incognito tabs via Intents -->
    <td>???</td><!-- from version -->
    <td>???</td><!-- to version -->
    <td>???</td><!-- Activity -->
    <td>???</td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>Privacy Browser</td><!-- name -->
    <td>com.stoutner.privacybrowser</td><!-- package name -->
    <td><a href="https://git.stoutner.com/?p=PrivacyBrowser.git;a=summary">yes</a></td><!-- source code available -->
    <td>???</td><!-- incognito tabs via Intents -->
    <td>???</td><!-- from version -->
    <td>???</td><!-- to version -->
    <td>???</td><!-- Activity -->
    <td>???</td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>Tint Browser</td><!-- name -->
    <td>org.tint</td><!-- package name -->
    <td><a href="https://github.com/Anasthase/TintBrowser">yes</a></td><!-- source code available -->
    <td>no</td><!-- incognito tabs via Intents -->
    <td></td><!-- from version -->
    <td></td><!-- to version -->
    <td></td><!-- Activity -->
    <td></td><!-- extra -->
    <td>no</td><!-- active project -->
</tr>
<tr>
    <td>Vivaldi</td><!-- name -->
    <td>com.vivaldi.browser</td><!-- package name -->
    <td><a href="https://vivaldi.com/blog/vivaldi-browser-open-source/">not really</a></td><!-- source code available -->
    <td>no</td><!-- incognito tabs via Intents -->
    <td></td><!-- from version -->
    <td></td><!-- to version -->
    <td></td><!-- Activity -->
    <td></td><!-- extra -->
    <td>yes</td><!-- active project -->
</tr>
<tr>
    <td>Zirco Browser</td><!-- name -->
    <td>org.zirco</td><!-- package name -->
    <td><a href="https://archive.softwareheritage.org/browse/origin/http://zirco-browser.googlecode.com/svn//directory/">yes</a></td><!-- source code available -->
    <td>no</td><!-- incognito tabs via Intents -->
    <td></td><!-- from version -->
    <td></td><!-- to version -->
    <td></td><!-- Activity -->
    <td></td><!-- extra -->
    <td>no</td><!-- active project -->
</tr>
</table>


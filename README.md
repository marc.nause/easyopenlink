# Easy Open Link
Easy Open Link is a small Android app which makes it easy to open links from text documents via the share function of many apps. No more cumbersome copy and paste. Easy Open Link also allows you to open several links at the same time.

1. Roughly select the URL(s). It does not matter if the selection also contains additional text or white spaces.
1. Press the "share" symbol.
1. Select "open link"

Instruction video:

[![click to start video](promo/0.jpg)](http://www.youtube.com/watch?v=cTIn11Zw1cc "Start video for 'Easy Open Link'")

# Permission
Easy Open Link requires the RECEIVE_BOOT_COMPLETED permission. I have written [an article](docs/permissions/RECEIVE_BOOT_COMPLETED.md) about the reasons for this.

# Binary
The app is available on:

[![Get it on Google Play](promo/en-play-badge_resized.png)](https://play.google.com/store/apps/details?id=de.audioattack.openlink)
[![Get it on F-Droid](promo/F-Droid-button_get-it-on.png)](https://f-droid.org/repository/browse/?fdid=de.audioattack.openlink)

APKs can be downloaded from:

[https://codeberg.org/marc.nause/easyopenlink/releases](https://codeberg.org/marc.nause/easyopenlink/releases)

# License
This software is licensed under the terms of the [GPL v3](LICENSE).

![logo GPL v3](promo/GPLv3.png)
